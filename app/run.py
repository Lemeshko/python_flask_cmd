from flask import Flask, Response, request, render_template
from shelljob import proc
import DB

app = Flask(__name__)
cmd = 'clear'

@app.route('/')
def my_form():

    return render_template('input.html')

#read insert command and add to DB
@app.route('/', methods=['GET', 'POST'])
def execute():
    global cmd
    cmd = request.form['cmdtext']
    if cmd != '':
        DB.addhistory(cmd)

    return render_template('input.html')

#shown command execturion
@app.route('/0', methods=['GET','POST'])
def excute_form():
    group = proc.Group()
    group.run(cmd)

    def generator():
        while group.is_pending():
            data = group.readlines()
            for handle, lines in data:
                yield lines
    return Response(generator(), mimetype='text/plain')

#shown history
@app.route('/history', methods=['GET','POST'])
def history():
    DB.db_connection
    query = "SELECT * FROM historysave"
    try:
        cursor = DB.db_connection.cursor()
        cursor.execute(query)
        data = cursor.fetchall()
        print("Records shown successfully from history")

    except mysql.connector.Error as error:
        DB.db_connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))

    return render_template("history.html", value=data)


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8000, debug=True)
