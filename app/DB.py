import mysql.connector

#DB configuration
config = {
        'user': 'root',
        'password': 'rootroot',
        'host': 'db',
        'port': '3306',
        'database': 'history'
}
#check DB and create if not created
db_connection = mysql.connector.connect(**config)
db_cursor = db_connection.cursor()
db_cursor.execute("CREATE DATABASE IF NOT EXISTS history")
db_cursor.execute("SHOW DATABASES")
#print all databases
for db in db_cursor:
	print(db)

#check table and create if not created
db_cursor = db_connection.cursor()
db_cursor.execute("CREATE TABLE IF NOT EXISTS historysave(cmd TEXT, time TIMESTAMP)")
db_cursor.execute("SHOW TABLES")
for table in db_cursor:
	print(table)



def addhistory(command):
    sql_insert_query = "INSERT INTO historysave (cmd) VALUES (%s)"
    insert_tuple = [(command)]
    try:
        db_connection = mysql.connector.connect(**config)
        db_cursor = db_connection.cursor(prepared=True)
        db_cursor.execute(sql_insert_query, insert_tuple)
        db_connection.commit()

        print ("Record inserted successfully into history")

    except mysql.connector.Error as error :
        db_connection.rollback()
        print("Failed to insert into MySQL table {}".format(error))

    finally:
        #closing database connection.
        if(db_connection.is_connected()):
            db_cursor.close()
            db_connection.close()
            print("MySQL connection is closed")