FROM python:3.6

#ADD . /app

WORKDIR /app
COPY app/requirements.txt /app

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY * /app/

CMD python run.py